import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-dashboard',
  templateUrl: './product-dashboard.component.html',
  styleUrls: ['./product-dashboard.component.css']
})
export class ProductDashboardComponent implements OnInit {

  listOfAllProducts: any;
  errorMessage: string;
  successMessage: string;

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.getAllProductsList();
  }

  private getAllProductsList() {
    this.productsService.getAllProducts().subscribe((resp: any) => {
      this.listOfAllProducts = resp.data;
    });
  }

  addToCart(productId: any) {
    let totalProductsInCart = this.productsService.getProductsAddedToCart();
    const isProductAlreadyAddedInCart = totalProductsInCart.find(ele => ele.id === productId);
    if (isProductAlreadyAddedInCart) {
      this.errorMessage = 'You cannot add the same Product Again';
      this.scrollToTop();
      setTimeout(() => {
        this.errorMessage = ''
      }, 4000);
    } else {
      this.productsService.addToCart(productId);
      totalProductsInCart = this.productsService.getProductsAddedToCart();
      this.productsService.numberOfItemsAddedInCart(totalProductsInCart.length);
      this.successMessage = 'Product Added Successfully In The Cart';
      this.scrollToTop();
      setTimeout(() => {
        this.successMessage = ''
      }, 4000);
    }
  }

  private scrollToTop() {
    window.scroll(0, 0);
  }
  
}
