import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product-checkout',
  templateUrl: './product-checkout.component.html',
  styleUrls: ['./product-checkout.component.css']
})
export class ProductCheckoutComponent implements OnInit {

  productDetails = [];
  isDetailsExist = false;
  noOfProductsInTheCart = 0;
  subscription: Subscription;

  constructor(private productsService: ProductsService) {
    this.subscription = this.productsService.getNoOfProductsAddedInCart().subscribe((data: any) => {
      this.noOfProductsInTheCart = data.itemsInCart;
    })
  }

  ngOnInit(): void {
  }

  viewProducts() {
    const productsToBeCheckedOut = this.productsService.getProductsAddedToCart();
    this.isDetailsExist = productsToBeCheckedOut.length > 0 ? true : false;
    this.productDetails = [];
    if (this.isDetailsExist) {
      for (let i = 0; i < productsToBeCheckedOut.length; i++) {
        const details = {};
        details['productId'] = productsToBeCheckedOut[i].id;
        details['productName'] = productsToBeCheckedOut[i].name;
        details['unitPrice'] = productsToBeCheckedOut[i].price;
        details['items'] = productsToBeCheckedOut[i].quantity;
        details['subTotal'] = Number(details['unitPrice']) * Number(details['items']);
        this.productDetails.push(details);
      }
    }
  }

  removeProduct(productId: any) {
    this.productsService.removeProduct(productId);
    this.noOfProductsInTheCart-= 1;
    this.viewProducts();
  }

}
