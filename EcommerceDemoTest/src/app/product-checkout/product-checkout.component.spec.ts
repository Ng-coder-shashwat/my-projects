import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProductsService } from '../products.service';
import { ProductCheckoutComponent } from './product-checkout.component';
import { of } from 'rxjs';

const mockData = {
  "data":[
    {
      "id": 1,
      "name": "Product 1",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "100.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 2,
      "name": "Product 2",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "130.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 3,
      "name": "Product 3",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "250.00",
      "quantity": "3",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 4,
      "name": "Product 4",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "180.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 5,
      "name": "Product 6",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "230.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 7,
      "name": "Product 7",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "10.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 8,
      "name": "Product 8",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "30.00",
      "quantity": "5",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    }
  ]
};

describe('ProductCheckoutComponent', () => {
  let component: ProductCheckoutComponent;
  let service: ProductsService;
  let fixture: ComponentFixture<ProductCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductCheckoutComponent ],
      imports: [HttpClientTestingModule],
      providers: [ProductsService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.inject(ProductsService);
    spyOn(service, 'getNoOfProductsAddedInCart').and.callFake(() => { return of({itemsInCart: 1}) });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.subscription).toBeDefined();
  });

  it('#viewProducts()', () => {
    spyOn(service, 'getProductsAddedToCart').and.returnValue(mockData.data);
    component.viewProducts();
    expect(component.isDetailsExist).toBeTruthy();
    expect(component.productDetails.length).toBeGreaterThan(0);
  });

  it('#viewProducts()', () => {
    spyOn(service, 'getProductsAddedToCart').and.returnValue([]);
    component.viewProducts();
    expect(component.isDetailsExist).toBeFalsy();
    expect(component.productDetails.length).toEqual(0);
  });

  it('#removeProduct()', () => {
    spyOn(component, 'viewProducts');
    spyOn(service, 'removeProduct');
    component.noOfProductsInTheCart = 5;
    component.removeProduct(2);
    expect(component.noOfProductsInTheCart).toEqual(4);
    expect(component.viewProducts).toHaveBeenCalled();
    expect(service.removeProduct).toHaveBeenCalledWith(2);
  });
});
