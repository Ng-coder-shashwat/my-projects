import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { ProductsService } from './products.service';


const mockResponse = {
  "data":[
    {
      "id": 1,
      "name": "Product 1",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "100.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 2,
      "name": "Product 2",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "130.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 3,
      "name": "Product 3",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "250.00",
      "quantity": "3",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 4,
      "name": "Product 4",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "180.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 5,
      "name": "Product 6",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "230.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 7,
      "name": "Product 7",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "10.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 8,
      "name": "Product 8",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "30.00",
      "quantity": "5",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    }
  ]
};

describe('ProductsService', () => {
  let service: ProductsService;
  let http: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductsService, HttpClient]
    });
    service = TestBed.inject(ProductsService);
    http = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getAllProducts(), method gets called and allProductList must have values', () => {
    service.getAllProducts().subscribe((resp: any) => {
      expect(resp).toEqual(mockResponse);
      expect(resp.length).toBeGreaterThan(0);
      expect(service.allProductsList).toEqual(resp);
    });
    const req = httpTestingController.expectOne(service.apiUrl);
    expect(req.request.method).toEqual('GET');
  });

  it('#addToCart(), method gets called and product Id exist in productList, added To CartList', () => {
    service.allProductsList = mockResponse;
    service.addToCart(2);
    expect(service.productsAddedToCart.length).toBeGreaterThan(0); 
  });

  it('#addToCart(), method gets called and product Id does not exist in productList, not added To CartList', () => {
    service.allProductsList = mockResponse;
    service.addToCart(78);
    expect(service.productsAddedToCart.length).toEqual(0);
  });

  it('#getProductsAddedToCart(), should return the products added in Cart', () => {
    service.getProductsAddedToCart();
    expect(service.productsAddedToCart.length).toEqual(0);
  });

  it('#removeProduct(), should remove Product from cart list if product id exist in cart list', () => {
    service.productsAddedToCart = mockResponse.data;
    service.removeProduct(1);
    expect(service.productsAddedToCart.length).toEqual(6);
  });

  it('#numberOfItemsAddedInCart()', () => {
    spyOn(service.subject, 'next');
    service.numberOfItemsAddedInCart(12);
    expect(service.subject.next).toHaveBeenCalled();
  });

  it('#getNoOfProductsAddedInCart()', () => {
    spyOn(service.subject, 'asObservable');
    service.getNoOfProductsAddedInCart();
    expect(service.subject.asObservable).toHaveBeenCalled();
  });
});
