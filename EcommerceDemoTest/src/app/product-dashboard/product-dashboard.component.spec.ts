import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProductsService } from '../products.service';
import { ProductDashboardComponent } from './product-dashboard.component';
import { of } from 'rxjs';

const mockData = {
  "data":[
    {
      "id": 1,
      "name": "Product 1",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "100.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 2,
      "name": "Product 2",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "130.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 3,
      "name": "Product 3",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "250.00",
      "quantity": "3",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 4,
      "name": "Product 4",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "180.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 5,
      "name": "Product 6",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "230.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 7,
      "name": "Product 7",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "10.00",
      "quantity": "10",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    },
    {
      "id": 8,
      "name": "Product 8",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "price": "30.00",
      "quantity": "5",
      "image": "http://truth-events.com/wp-content/uploads/2019/09/dummy.jpg"
    }
  ]
};

describe('ProductDashboardComponent', () => {
  let component: ProductDashboardComponent;
  let fixture: ComponentFixture<ProductDashboardComponent>;
  let service: ProductsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductDashboardComponent ],
      imports: [HttpClientTestingModule],
      providers: [ProductsService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.inject(ProductsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#ngOnInit()', () => {
    spyOn(service, 'getAllProducts').and.callFake(() => {
      return of(mockData);
    });
    component.ngOnInit();
    expect(component.listOfAllProducts).toEqual(mockData.data);
  });

  it('#addToCart(), if product Id is not present in the cart', fakeAsync(() => {
    spyOn(service, 'getProductsAddedToCart').and.returnValue(mockData.data);
    spyOn(service, 'addToCart');
    spyOn(service, 'numberOfItemsAddedInCart');
    spyOn(window, 'scroll');
    component.addToCart(12);
    tick(4000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.successMessage).toBe('');
    });
    expect(service.addToCart).toHaveBeenCalledWith(12);
    expect(service.getProductsAddedToCart).toHaveBeenCalled();
    expect(service.numberOfItemsAddedInCart).toHaveBeenCalled();
    expect(window.scroll).toHaveBeenCalled();
  }));

  it('#addToCart(), if product Id is present in the cart', fakeAsync(() => {
    spyOn(service, 'getProductsAddedToCart').and.returnValue(mockData.data);
    spyOn(window, 'scroll');
    component.addToCart(1);
    tick(4000);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.errorMessage).toBe('');
    });
    expect(service.getProductsAddedToCart).toHaveBeenCalled();
    expect(window.scroll).toHaveBeenCalled();
  }));
});
