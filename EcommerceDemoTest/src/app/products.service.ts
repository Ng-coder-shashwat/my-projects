import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  productsAddedToCart = [];
  allProductsList: any;
  apiUrl = 'https://www.mocky.io/v2/5eda4003330000740079ea60';
  subject = new Subject<any>();

  constructor(private http: HttpClient) { }

  getAllProducts() {
    return this.http.get(this.apiUrl).pipe(
      map((response) => {
        this.allProductsList = response;
        return response;
      }));
  }

  addToCart(productId: any): any {
    const list = this.allProductsList.data;
    const productObj: any = list.find(ele => ele.id === productId);
    if (productObj) {
     this.productsAddedToCart.push(productObj);
    }
  }

  getProductsAddedToCart() {
    return this.productsAddedToCart;
  }

  removeProduct(productId: any) {
    const indexOfProductPresent = this.productsAddedToCart.map(function(item) { return item.id}).indexOf(productId);
    this.productsAddedToCart.splice(indexOfProductPresent, 1);
  }

  numberOfItemsAddedInCart(items: number) {
    this.subject.next({itemsInCart: items});
  }

  getNoOfProductsAddedInCart(): Observable<any> {
    return this.subject.asObservable();
  }
}
